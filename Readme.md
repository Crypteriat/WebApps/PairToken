# Pair: A Foundation for Decentralized Finance (DeFi) 

This is a project page makes the case for a DeFi-based Silver-Gold ratio oracle and synthetic financial instrument.
We propose a cross-chain atomic transaction core implementation leveraging smart contracts, including new
[DLCs](https://medium.com/interdax/discreet-log-contracts-smart-contracts-for-bitcoin-d75f22d25dac) and [PTLCs](https://bitcoinops.org/en/topics/ptlc/)
technologies. These contracts are capable of maintaining synchronized state between different blockchains. By doing so, a token may be offered across the
cryptocurrency ecosystem without depending on a specific blockchain implementation.  Currently, DeFi suffers from both smart contract programming risk
as well as execution risk. Congestion at the lowest layer may prevent execution within a block time. Therefore, another form of risk beyond smart contract
risk exists on DeFi. Limit orders, as well as other trades, may not be fulfilled during periods of congestion.

risk contracts blockchains may not execute predetermined trades on the network. By using atomic
swaps and low-latency services, trades have a much higher probability of executing than traditional best-effor single chain approaches.

<p align="center;"><img alt="" src="./docs/drawings/CrossChain.png" width="670"></img></p>

In this project, we propose to create a core smart contract which can return yield by creating a synthetic asset which both acts as an oracle for the silver-gold ratio
as well as creating an automated market maker for both that asset pair as well as derivatives from that pair.
Ideal assets for automated market making systems are [highly correlated](https://medium.com/dragonfly-research/what-explains-the-rise-of-amms-7d008af1c399#4e95).
This includes silver, gold and many crypto currency pairs. We're focused on developing open source software which can be integrated into precious metals depository operations.
Ideally, this DeFi code will permnanently eliminate "paper market" pricing manipulation of commodities, including precious metals ([see GATA.org](https://gata.org)). 
DeFi offers an ideal platform since it completely exists outside of the official sector and, with proper governance, can enforce physical existence of the underlying
commodity.

#### Outline of logic:

1. [Relationship between latency, consensus and delay diameter](#relationship-between-latency-consensus-network-degree-and-delay-diameter)
2. [Implications: Low Latency provides blockchains with fundamental advantage.](#Implications:-Low-Latency-provides-blockchains-with-fundamental-advantages)
3. [Decomposing DeFi's basic financial instruments](#Decomposing-DeFi's-financial-foundations)
4. [Challenges with the Current DeFi Ecoystem](#DeFi-Challenges)
5. [Liquidity Pools provide nearly riskless yield (at least in Crypto terms)](#Liquidity-Pools-provide-nearly-riskless-yield)
6. [Dynamic Automated Market Makers now leverage Oracles (see Bancor v2) for greater profitability.](#DAMMs)
7. [Tokenized Depositories (e.g. precious metals, arts, other crypto currencies)Business Models](#tokenized-depository)
8. [Natural Liquidity Pools are emerging for highly correlated assets](#liquidity-pool)
9. [Gold and Silver are ideal pairs for Liquidity Pools (LPs)](#Corelation)
10. [A Natural Silver-Gold ratio LP that sits outside the paper markets will emerge.](#PaperMarkets)
11. [DeFi components will inevitably incorporate leverage into their smart contracts.](DeFi-components-will-inevitably-incorporate-leverage-into-their-smart-contracts) 
12. [Economies CANNOT NOT have leverage. Key financial metrics must be capable of measuring sentiment, and thus leverage, for whole ecosystem.](Leverage)

#### tl;dr: By setting service and exchange fees high enough, all existing precious metals depository operators can tokenize their assets and cover long-term expenses.
  In other words, a customer will simply be able to purchase gold and silver, mint it into a synthetic, and hold it with yield -- no storage or maintenance
  fees will accrue as long as they leave the asset in the pool. Incentivizing the holding of the asset also overcomes one of the key issues on DeFi today: execution risk.


#### [Relationship between latency, consensus, network degree and delay diameter](#relationship-between-latency-consensus-network-degree-and-delay-diameter)

  Few distributed computing experts design their systems for reliable networks. In fact Vitalik Buterin has been [interviewed](https://www.youtube.com/watch?v=gJq9FcYZe0k&feature=youtu.be&t=2160) emphasizing that Ethereum's design does not presume a reliable network with bounded latency or packet loss. However,
  there are pragmatic decisions and bounds which will be determined though network delay, and degree (random to scale-free). The the degree of the network and  information transfer delay obviously
  has an impact on consensus intervals and oracle quality. This is an [open area of research](https://www.mdpi.com/2504-3900/28/1/6/pdf) without a generalized model, currently. We can be certain that faster networks create faster rates of consensus and
  produce higher quality information from oracles. We can also be certain that the new tiered Internet, offering lower latency and packet loss services, will also be permissioned. Thus, the permissionless assumptions 
  made within the DeFi ecosystem and most blockchain projects presume much more reliable networks than those that are likely to exist in a permissioned era. With the elimination of "net neutrality," commercial
  interests along with regulatory authorities will have to unravel the policies related to acquiring permissioned access.

#### [Implications: Low Latency provides blockchains with fundamental advantages.](#Implications:-Low-Latency-provides-blockchains-with-fundamental-advantages)

  Therefore, blockchains which are permissioned to use higher quality network services will have non-organic advantages. The crypto community, in general, should understand this point clearly. 
  Government issued currencies will likely garner a superior level of access to the Internet. In addition, we must also assume that regulators may choose to disadvantage other blockchains and DeFi services by
  delaying their traffic in favor of higher priority government traffic. In other words, it should be assumed within the DeFi community that "speed bump" regulatory capabilities will come into DeFi just like
  they did for Centralized Finance (CeFi). The regulatory characteristics will take a different but with much of the same effect.

#### [Decomposing DeFi's financial foundations](#Decomposing-DeFi's-financial-foundations)

  We agree with Raoul Pal when he said: ["a new financial system will rise up... from the hive mind"](https://youtu.be/LGR8VmW6p8c?t=5005). As such, we think DeFi represents the complete
  replacement of our currently CeFi system within 20 years. At a base level, DeFi began with swaps (specifically [Uniswap](https://uniswap.org)). Swaps represent the foundation on DeFi. Synthetics, exotics, and yield accounts
  leverage this nearly frictionless exchange system.

#### [Challenges with the Current DeFi Ecoystem](#DeFi-Challenges)

  Currently, there are several stable coin implementations available for DeFi. These include []Maker DAO's DAI](https://makerdao.com/en/) as well as custodial systems such as [Tether (USDT)](https://tether.to/) and [USDC](https://www.circle.com/en/usdc). We think there are inherent weakenesses for 
  both of these types of systems. As a "centralized" implementation, custodial systems have obvious disadvantages. The terms of service and usage dictate that tokens can be invalidated and the service is essentially centralized. "Decentralized" approaches such as Maker have become increasingly centralized
  through their collateralization approach. The DAI can now be backed via collateral such as USDC. This creates a number of inherent problems including potential reversal of USDC custodianship. Fundamentally, the DeFi ecoystem requires a non-fiat based stable coin. Silver and gold represent the best
  and longest used collateralization methods. Therefore, we support creating a decentralized model similar to DAI without the collateralization concerns of using underlying custodial assets.

#### [Liquidity Pools provide nearly riskless yield (at least in Crypto terms)](#Liquidity-Pools-provide-nearly-riskless-yield))

  Liquidity pools represent a core component of DeFi. In addition to [yield farming](https://youtu.be/9vTaNl2_B8A) and [depository](https://youtu.be/9vTaNl2_B8A), these pools provide nearly riskless returns to pool holders. Automated market
  making allows holders to receive yields well in excess of the financially repressed traditional markets. Of course, new forms of risk are inherently part of DeFi such as oracle, protocol,
  and infrastructural.

  New distirbuted Zero Knowledge proof systems are now emerging that will allow state channel networks to emerge. These systems can create and manage collective "liquidity pools." Hence,
  a network effect may be built along with routing through a network of state channels systems with subsidies provided to both node operators and liquidity providers. 

<img alt="" src="./docs/drawings/ZK_Liquidty_pool.png"  width="670">

#### [Dynamic Automated Market Makers now leverage Oracles for greater profitability.](#DAMMs)

Dynamic Automated Market Makers (or DAMMs) provide almost riskless yield but can suffer impermanent loss. If liquidity providers withdraw their funds before a market maker recovers,
the liquidity pool can be drained.

<img alt="" src="./docs/drawings/DAMMs.png"  width="670">

Currently, there are two approaches to the use of oracles. Bancor v2, for example, now uses information from oracles to improve earning retention by adapting their
[bonding curves.](https://youtu.be/ouLLi_nYScc?t=74) Meanwhile, protocols such as Uniswap are now acting as [paid oracles.](https://medium.com/@epheph/using-uniswap-v2-oracle-with-storage-proofs-3530e699e1d3).
We propose to leverage the new dynamic nature of market makers using new permissioned oracle services. By having the fastest possible price discovery via the oracle, this projects
automated market maker can offer better profitability than current participants.


#### [Tokenized Depository (e.g. precious metals, arts, other crypto currencies) Business Models](#tokenized-depository)

There are three primary expenses for depository operators: administration, insurance, and security; DeFi governance can be tokenized and valued as well. Of course, crypto currency storage may be a bit different in
nature than physical goods, a depository operator must be concerned with insuring their holdings,  and maintaining security. Administration and governance costs will enter the equation as well. 
Smart contract security audits have become key to any reputable project. As we move forward, the crypto-operational costs will become more
predictable and that information can be combined with the opearational costs associated the physical operations of a depository. We assert that once projects can utilize this information, we will see liquidity pools emerge that
can provide enough yield to cover all operating expenses. Therefore, regular consumers, institutions, as well as governments will be able to hold a combinations of gold and silver which returns predictable yield.
The market should greatly expand since a silver-gold ratio token is far more accessible, portable, and provides stable coin characteristics.

#### [Natural Liquidity Pools are emerging for highly correlated assets](#liquidity-pool)

#### [Gold and Silver are ideal pairs for Liquidity Pools (LPs)](#Correlation)

#### [A Natural Silver-Gold ratio LP that sits outside the paper markets will emerge](#PaperMarkets).

#### [DeFi components will inevitably incorporate leverage into their smart contracts.](DeFi-components-will-inevitably-incorporate-leverage-into-their-smart-contracts)

  We propose to create a transparent smart contract framework that is sponsored by the SEC as part of its code.gov project.

<img alt="" src="./docs/drawings/DeFi_Bank_Flow.png"  width="670">

#### [No economy CANNOT NOT have leverage. (We use the triple negative to make a point.) The key is to provide transparent financial metrics for the whole ecosystem.](Leverage)

An economy CANNOT NOT have leverage. (We use a double negative to make a point.) The key is to provide transparent financial information and
[efficient clearing](https://www.econlib.org/archives/2012/01/an_answer_to_a.html) for the economy. When a derivative is created without sufficient
collateral, markets can instantly determine how much leverage exists in the overall economy. With DeFi and this form of leverage, derivatives are required to be
collateralized. Typically, if the market prices move to threaten required thresholds, parties are required to deposit more collateral or lose their entire derivative
contract. Most DeFi protocols require collateral to be deposited within an hour of threshold violations. Therefore, DeFi markets are significantly more transparent and
offer inherently less counter-party risk than traditional CeFi markets.

### Participation
Participants may contribute to the community either as a developer, token owner, or governor. Governors represent depository operators in the system.
We do not define the role of governors or how smart contract interaction will emerge to create an overall governance model or organizational structure.
This project is entirely open source. We intend to adhere to the principles of voluntary participation both in terms of development of the
service as well as prosletyzing its capabilities. We want the broader community to make use of this project to enable greater transparency within the DeFi and
CeFi sectors. Feel free to submit PRs if you are these messages reasonate with our message and are following similar paths.

## [Architecture](https://gitlab.com/Crypteriat/defi_sgr_pool/-/blob/master/docs/Architecture.md)

## [Operational Logistics](https://gitlab.com/Crypteriat/defi_sgr_pool/-/blob/master/docs/Operational_Logistics.md)

## [Formal Association](https://gitlab.com/Crypteriat/defi_sgr_pool/-/blob/master/docs/Formal_Association.md)

## [Frequently Asked Questions](https://gitlab.com/Crypteriat/defi_sgr_pool/-/blob/master/docs/FAQ.md)

## [License](https://www.gnu.org/licenses/gpl-3.0.en.html)

