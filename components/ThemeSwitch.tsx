import { useState, useEffect } from 'react';
const darkMode = require('/tailwind.config').darkMode;

function ThemeSwitch() {
  const root =
    typeof window === 'undefined'
      ? { classList: { toggle: () => null }, getElementsByClassName: () => null }
      : window.document.documentElement;
  const [dark, setDark] = useState(true);

  
  useEffect(() => {
    window.document.documentElement.classList.add('dark');
  }, []);
 

  if (!darkMode) {
    return null;
  }
  return (
    <button
      aria-label="Toggle Theme Dark/Light"
      type="button"
      className="w-8 h-8 p-1 ml-1 mr-1 rounded sm:ml-4"
      onClick={() => {
        root.classList.toggle('dark');
        setDark(!dark);
      }}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
        fill="currentColor"
        className="text-gray-900 dark:text-gray-100"
      >
        {dark ? 
          (
            <path
              fillRule="evenodd"
              d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z"
              clipRule="evenodd"
            />
          )
          :
          ( <path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z" />)
        }
      </svg>
    </button>
  );
}

export default ThemeSwitch;

/*
"dependencies": {
    "@tailwindcss/forms": "^0.4.0",
    "@tailwindcss/typography": "^0.5.2",
    "@trpc/client": "^9.23.2",
    "@trpc/react": "^9.23.2",
    "autoprefixer": "10.4.5",
    "axios": "^0.27.2",
    "eslint-config-react-app": "^7.0.1",
    "eslint-plugin-jsx-a11y": "^6.5.1",
    "eslint-plugin-react": "^7.30.0",
    "i": "^0.3.7",
    "next": "^12.1.0",
    "next-auth": "^4.3.2",
    "next-i18next": "^10.4.0",
    "next-seo": "^5.1.0",
    "postcss": "^8.4.7",
    "preact": "^10.6.6",
    "react": "^17.0.2",
    "react-app": "^1.1.2",
    "react-dom": "^17.0.2",
    "react-fade-in": "^2.0.1",
    "react-query": "^3.39.0",
    "tailwind-float-label": "^2.1.0",
    "tailwindcss": "^3.0.23"
  },
  "devDependencies": {
    "@next/bundle-analyzer": "^12.1.0",
    "@svgr/webpack": "^6.2.1",
    "@types/react": "^17.0.39",
    "eslint": "^8.13.0",
    "file-loader": "^6.2.0",
    "prop-types": "^15.8.1",
    "typescript": "^4.5.5",
    "webpack": "^5.69.1"
  }
  */