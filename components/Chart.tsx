import React, { useEffect, useState } from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsExporting from 'highcharts/modules/exporting';
import HighchartsReact from 'highcharts-react-official';
import moment from 'moment';
import { getHistoricalPairPrices } from '../lib/utils';

export default function Chart() {
  if (typeof Highcharts === 'object') {
    HighchartsExporting(Highcharts);
  }

  const [currency, setCurrency] = useState('USD');
  const [data, updateData] = useState<number[][]>([]);

  let screenWidth = 750;
  if (typeof window !== 'undefined') {
    screenWidth = window.innerWidth;
    // console.log(screenWidth)
  }

  useEffect(() => {
    const getData = async () => {
      const data = await getHistoricalPairPrices(currency);
      updateData(data);
    };
    getData();
  }, [currency]);

  const options = {
    style: 'currency',
    currency: currency,
    minimumFractionDigits: 2,
    maximumFractionDigits: 8,
  };
  const numberFormat = new Intl.NumberFormat(
    currency === 'JPY' ? 'ja-JP' : currency === 'XOF' ? 'ff-SN' : 'en-US',
    options
  );

  const chartConfig = {
    accessibility: {
      enabled: false,
    },
    yAxis: [
      {
        offset: 20,
        // min: 0.5,
        // max: 3,

        labels: {
          formatter: function () {
            //@ts-expect-error
            return numberFormat.format(this.value);
          },
          x: -15,
          style: {
            color: '#000',
            position: 'absolute',
          },
          align: 'left',
        },
        title: {
          text: currency,
        },
      },
    ],
    tooltip: {
      shared: true,
      formatter: function () {
        return (
          //@ts-expect-error
          numberFormat.format(this.y) +
          '</b><br/>' +
          //@ts-expect-error
          moment(this.x).format('MMMM Do YYYY, h:mm')
        );
      },
    },
    plotOptions: {
      series: {
        showInLegend: true,
        // gapSize: 6,
      },
    },
    title: {
      text: 'PAIR Stablecoin',
    },
    chart: {
      height: 600,
    },

    credits: {
      enabled: false,
    },

    legend: {
      enabled: true,
    },
    exporting: {
      menuItemDefinitions: {
        BTC: {
          text: 'BTC',
          onclick: () => {
            setCurrency('BTC');
          },
        },
        ETH: {
          text: 'ETH',
          onclick: () => {
            setCurrency('ETH');
          },
        },
        ETC: {
          text: 'ETC',
          onclick: () => {
            setCurrency('ETC');
          },
        },
        EUR: {
          text: 'EUR',
          onclick: () => {
            setCurrency('EUR');
          },
        },
        USD: {
          text: 'USD',
          onclick: () => {
            setCurrency('USD');
          },
        },
        JPY: {
          text: 'JPY',
          onclick: () => {
            setCurrency('JPY');
          },
        },
        XOF: {
          text: 'XOF',
          onclick: () => {
            setCurrency('XOF');
          },
        },
        // BNB: {
        //   text: 'BNB',
        //   onclick: () => {
        //     setCurrency('BNB');
        //   },
        // },
      },
      buttons: {
        contextButton: {
          symbol: 'menu',
          menuItems: [
            'viewFullscreen',
            'printChart',
            'separator',
            'downloadPDF',
            'downloadPNG',
            'downloadJPEG',
            'downloadSVG',
            'separator',
            'downloadXLS',
            'downloadCSV',
          ],
        },
        toggle1: {
          text: 'Currency: ' + currency,
          // className: 'toggleButton',
          align: 'center',
          x: screenWidth < 430 ? -5 : -150,
          y: 30,
          theme: {
            'stroke-width': 0.5,
            stroke: '#000000',
            r: 2,
          },
          menuItems: ['USD', 'EUR', 'JPY', 'XOF', 'BTC', 'ETH', 'ETC', 'BNB'],
        },
      },
    },
    // xAxis: [
    //   {
    //     title: {
    //       text: 'Time',
    //     },
    //   },
    // ],
    rangeSelector: {
      enabled: true,
      buttons: [
        {
          type: 'day',
          count: 7,
          text: '7d',
        },
        {
          type: 'month',
          count: 1,
          text: '1m',
        },
        {
          type: 'month',
          count: 3,
          text: '3m',
        },
        {
          type: 'year',
          count: 1,
          text: '1y',
        },
        {
          type: 'all',
          text: 'All',
        },
      ],
      selected: 4,
      inputEnabled: true,
      dropdown: 'always',
      x: 10,
      y: -5,
      inputPosition: {
        align: 'right',
        y: screenWidth > 390 && screenWidth < 600 ? 40 : 0,
      },
    },
    series: [
      {
        name: 'Price',
        type: 'spline',

        data: data,
        // tooltip: {
        //   valueDecimals: 2,
        // },
      },
    ],
  };

  return (
    <HighchartsReact
      highcharts={Highcharts}
      constructorType={'stockChart'}
      options={chartConfig}
    />
  );
}
