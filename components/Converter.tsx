import { useTranslation } from 'next-i18next';
import { useState, useEffect } from 'react';
import { getLatestMetalData, getExchangeRates } from '@lib/utils';

export default function Converter() {
  const { t } = useTranslation('converter');

  const [refreshClick, setRefreshClick] = useState(true);
  const [dropwdownValue, setDropdownValue] = useState('USD');
  const [dropdownData, setDropdowndata] = useState([
    { label: 'USD', price: 0, symbol: '$' },
    { label: 'EUR', price: 0, symbol: '€' },
    { label: 'JPY', price: 0, symbol: '¥' },
    { label: 'BTC', price: 0, symbol: '' },
    { label: 'ETH', price: 0, symbol: '' },
    { label: 'ETC', price: 0, symbol: '' },
  ]);

  const handleChange = (event) => {
    setDropdownValue(event.target.value);
  };

  // Update metal prices on refresh click
  useEffect(() => {
    const getMetalData = async () => {
      const metalData = await getLatestMetalData();

      const goldGramPrice = metalData.lastGoldGramPrice;
      const silverGramPrice = metalData.lastSilverGramPrice;
      const goldPriceOz = goldGramPrice * 31.103;
      const silverPriceOz = silverGramPrice * 31.103;
      const goldSilverRatio = +(goldPriceOz / silverPriceOz).toFixed(2);

      const pairPriceUSD =
        (1 / goldSilverRatio) * goldGramPrice +
        (1 - 1 / goldSilverRatio) * silverGramPrice;

      const exchangeRates = await getExchangeRates();
      setDropdowndata([
        { label: 'USD', price: pairPriceUSD, symbol: '$' },
        { label: 'EUR', price: pairPriceUSD * exchangeRates.EUR, symbol: '€' },
        { label: 'JPY', price: pairPriceUSD * exchangeRates.JPY, symbol: '¥' },
        { label: 'BTC', price: pairPriceUSD * exchangeRates.BTC, symbol: '' },
        { label: 'ETH', price: pairPriceUSD * exchangeRates.ETH, symbol: '' },
        { label: 'ETC', price: pairPriceUSD * exchangeRates.ETC, symbol: '' },
      ]);
    };

    getMetalData();
    // console.log('refreshed')
  }, [refreshClick]);

  return (
    <>
      <div className="flex flex-col items-center mt-16">
        <h1 className="text-4xl">
          {t('title')}
          <select
            className="bg-transparent border-none text-4xl"
            value={dropwdownValue}
            onChange={handleChange}
          >
            {dropdownData.map((option) => (
              <option
                className="dark:bg-slate-700"
                key={option.label}
                value={option.label}
              >
                {option.label}&nbsp;
              </option>
            ))}
          </select>
        </h1>

        <div className="flex flex-row items-center pt-12">
          <p className="pr-1 text-4xl w-[12rem]">
            {dropdownData.find((e) => e.label === dropwdownValue)?.symbol ? (
              <>
                {dropdownData.find((e) => e.label === dropwdownValue)?.symbol}
                {dropdownData
                  .find((e) => e.label === dropwdownValue)
                  ?.price.toFixed(2)}
              </>
            ) : (
              <>
                {dropdownData
                  .find((e) => e.label === dropwdownValue)
                  ?.price.toFixed(6)}
              </>
            )}
          </p>
          <button
            className={
              'text-md font-bold uppercase px-6 py-2 w-auto rounded block dark:text-green-500 bg-indigo-400 dark:bg-indigo-700'
            }
            onClick={() => setRefreshClick(!refreshClick)}
          >
            {t('refresh')}
          </button>
        </div>
      </div>
    </>
  );
}
