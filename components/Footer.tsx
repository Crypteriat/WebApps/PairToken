import siteMetadata from '@data/siteMetadata';
import SocialIcon from '@components/social-icons';
import Link from './Link.js';
import { useTranslation } from 'next-i18next';

export default function Footer() {
  const { t } = useTranslation('footer');

  return (
    <footer>
      <div className="flex flex-col items-center mt-16">
        <div className="flex mb-3">
          <SocialIcon kind="gitlab" href={siteMetadata.gitlab} size={6} />
          <SocialIcon kind="mail" href={siteMetadata.email} size={6} />
          <SocialIcon kind="twitter" href={siteMetadata.twitter} size={6} />
        </div>
        <div className="flex mb-2 text-sm text-gray-500 space-x-2 dark:text-gray-400">
          <Link href="/">{siteMetadata.title} </Link>
        </div>
        <div className="mb-2 text-sm text-gray-500 dark:text-gray-400">
          <div>
            {t('footer')}{' '}
            <Link href="https://tailwindcss.com/">TailwindCSS,</Link>
            <Link href="https://nextjs.org/"> NextJS</Link> &{' '}
            <Link href="https://reactjs.org/">React</Link>
          </div>
        </div>
      </div>
    </footer>
  );
}
