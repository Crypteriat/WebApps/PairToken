import React, { useState } from 'react';

export default function Form(props: {
  [x: string]: any;
  name: any;
  setValue: (value: React.SetStateAction<number>) => void;
}) {
  const { name, setValue, ...other } = props;

  const [input, setInput] = useState('');

  const convert = (event) => {
    event.preventDefault();

    const valueNum = Number(input);
    setValue(valueNum);
  };

  return (
    <form onSubmit={convert}>
      <input
        {...other}
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
      />
    </form>
  );
}
