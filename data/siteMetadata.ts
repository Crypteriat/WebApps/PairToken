const siteMetadata = {
  title: 'PAIR',
  author: 'Crypteriat, Inc.',
  headerTitle: 'PAIR Stablecoin-Index',
  description: "A transparent Numéraire and Stablecoin Bitcoin and DeFi",
  language: 'en-us',
  siteUrl: 'https://pair.money',
  siteRepo: 'https://gitlab.com/crypteriat/webapp/Bandwidth.Fi',
  documentation: 'https://gitlab.com/Crypteriat/Compose_DAO#compose-able-decentralized-autonomous-organization-dao-a-framework-and-fund-for-dao-creation',
  googleanalytics: 'G-90TK21M9C3',
  image: '/static/img/avatar.png',
  logo: '/static/img/open-spectrum.png',
  btc: '/static/img/BTC.png',
  btcAddr: '3QfVhBcyCsi7gBvv9zVxvh8HnGfo1dEfUE',
  btcLink:
    'https://blockchain.com/btc/address/3QfVhBcyCsi7gBvv9zVxvh8HnGfo1dEfUE',
  eth: '/static/img/ETH.png',
  ethAddr: '0x6520a28adcd29C8E52007957160E5d2aEbF32a12',
  ethLink:
    'https://etherscan.io/address/0x6520a28adcd29C8E52007957160E5d2aEbF32a12',
  paypal: '/static/img/PayPalQR.png',
  paypalLink:
    'https://www.paypal.com/donate/?cmd=_s-xclick&hosted_button_id=WTVXDGXPUYMF6',
  paypallogo: '/static/img/btc.jpg',
  socialBanner: '/static/img/twitter-card.png',
  email: '5647553-jlt-crypteriat@users.noreply.gitlab.com',
  github: 'https://github.com/ComposeDAO',
  gitlab: 'https://gitlab.com/ComposeDAO',
  twitter: 'https://twitter.com/realCrypteriat',
  linkedin: 'https://www.linkedin.com/in/crypteriat',
  locales: [{ en: 'English' }, { fr: 'français' }],
}
export default siteMetadata;
