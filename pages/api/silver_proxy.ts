import fetch from 'node-fetch';

module.exports = async (req, res) => {
  const { startDate, endDate, apiKey } = req.query;
  const url = `https://data.nasdaq.com/api/v3/datasets/LBMA/SILVER?column_index=1&start_date=${startDate}&end_date=${endDate}&api_key=
${apiKey}`;

  const response = await fetch(url);
  const data = await response.json();

  res.status(200).json(data);
};
