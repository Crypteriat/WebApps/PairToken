import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import Chart from '@components/Chart';
import Converter from '@components/Converter';
import siteMetadata from '@data/siteMetadata';
import { PageSeo } from '@components/SEO';

export async function getStaticProps({
  locale = Object.keys(siteMetadata.locales[0])[0],
}) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'index', 'footer', 'converter'])),
    },
  };
}

export default function Home() {
  const { t } = useTranslation('index');

  return (
    <>
      <PageSeo
        title={siteMetadata.title}
        description={siteMetadata.description}
        url={siteMetadata.siteUrl}
      />

      <div className="text-center divide-y pt-8 divide-gray-200 dark:divide-gray-700">
        <h1 className="text-2xl sm:text-md leading-2 dark:text-gray-300">
          {t('header')}
        </h1>
      </div>

      <div className="flex flex-col py-10 xl:flex-row xl:justify-between">
        <div className="grow">
          <Chart></Chart>
        </div>
        <div className="flex flex-col gap-4 text-center xl:grid xl:grid-rows-2 xl:grid-flow-col xl:items-center xl:ml-8">
          <p className="text-lg p-3 border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md mt-20 xl:mt-0">
            {t('natural')}
          </p>
          <p className="text-lg p-3 border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md mt-10 xl:mt-0">
            {t('fully')}
          </p>
          <p className="text-lg p-3 border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md mt-10 xl:mt-0">
            {t('fragile')}
          </p>
          <p className="text-lg p-3 border-2 border-gray-200 border-opacity-60 dark:border-gray-700 rounded-md mt-10 xl:mt-0">
            {t('immune')}
          </p>
        </div>
      </div>

      <div className="">
        <Converter />
      </div>

      <div className="text-center pt-36">
        <a
          href={siteMetadata.documentation}
          target="_blank"
          rel="noreferrer"
          className="text-2xl sm:text-md leading-2 dark:text-gray-300 hover:text-blue-400 dark:hover:text-blue-400"
        >
          {t('documentation')}
        </a>
      </div>
    </>
  );
}
