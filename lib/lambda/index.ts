import aws from 'aws-sdk';
// import 'dotenv/config';
import {
  getMetalDataFromS3,
  getLatestMetalData,
  getExchangeRates,
  getPairDataFromS3,
} from './get_functions';

aws.config.update({
  // accessKeyId: process.env.NEXT_PUBLIC_ACCESS_KEY_ID,
  // secretAccessKey: process.env.NEXT_PUBLIC_SECRET_ACCESS_KEY,
  accessKeyId: process.env['ACCESS_KEY_ID'],
  secretAccessKey: process.env['SECRET_ACCESS_KEY'],
  region: 'us-east-1',
});

const s3 = new aws.S3({ apiVersion: '2006-03-01' });

async function updateMetalData() {
  const goldGramData: number[][] = await getMetalDataFromS3('goldGram.json');
  const silverGramData: number[][] = await getMetalDataFromS3(
    'silverGram.json'
  );
  const ratioData: number[][] = await getMetalDataFromS3('ratio.json');
  const latestMetalData = await getLatestMetalData();

  //Gold and Silver Gram Data
  goldGramData.push([
    latestMetalData.goldTimestamp,
    latestMetalData.lastGoldPrice,
  ]);
  silverGramData.push([
    latestMetalData.silverTimestamp,
    latestMetalData.lastSilverPrice,
  ]);

  const goldParams = {
    Bucket: 'pair-prices/metals',
    Key: 'goldGram.json',
    Body: JSON.stringify(goldGramData),
  };
  const silverParams = {
    Bucket: 'pair-prices/metals',
    Key: 'silverGram.json',
    Body: JSON.stringify(silverGramData),
  };

  s3.upload(goldParams, function (err, data) {
    if (err) {
      throw err;
    }
    console.log(`File uploaded successfully. ${data.Location}`);
  });

  s3.upload(silverParams, function (err, data) {
    if (err) {
      throw err;
    }
    console.log(`File uploaded successfully. ${data.Location}`);
  });

  //Gold-Silver Ratio Data
  const currGoldSilverRatio = +(
    latestMetalData.lastGoldPrice / latestMetalData.lastSilverPrice
  ).toFixed(2);
  ratioData.push([latestMetalData.goldTimestamp, currGoldSilverRatio]);

  const ratioParams = {
    Bucket: 'pair-prices/metals',
    Key: 'ratio.json',
    Body: JSON.stringify(ratioData),
  };

  s3.upload(ratioParams, function (err, data) {
    if (err) {
      throw err;
    }
    console.log(`File uploaded successfully. ${data.Location}`);
  });
}

async function calculatePairUSD() {
  const PAIR_USD_DATA = await getPairDataFromS3('pairUSD.json');
  const GOLD_GRAM_DATA = await getMetalDataFromS3('goldGram.json');
  const SILVER_GRAM_DATA = await getMetalDataFromS3('silverGram.json');
  const RATIO_DATA = await getMetalDataFromS3('ratio.json');

  const lastGoldGramPrice = GOLD_GRAM_DATA[GOLD_GRAM_DATA.length - 1][1];
  const lastSilverGramPrice = SILVER_GRAM_DATA[SILVER_GRAM_DATA.length - 1][1];
  const lastGoldSilverRatio = RATIO_DATA[RATIO_DATA.length - 1][1];
  const lastestTimestamp = RATIO_DATA[RATIO_DATA.length - 1][0];

  const pairPrice =
    (1 / lastGoldSilverRatio) * lastGoldGramPrice +
    (1 - 1 / lastGoldSilverRatio) * lastSilverGramPrice;
  PAIR_USD_DATA.push([lastestTimestamp, pairPrice]);

  const pairParams = {
    Bucket: 'pair-prices',
    Key: 'pairUSD.json',
    Body: JSON.stringify(PAIR_USD_DATA),
  };

  s3.upload(pairParams, function (err, data) {
    if (err) {
      throw err;
    }
    console.log(`File uploaded successfully. ${data.Location}`);
  });
}

async function calculatePairPrices() {
  const rates = await getExchangeRates();
  const PAIR_USD_DATA = await getPairDataFromS3('pairUSD.json');
  const latestPairUSDPrice = PAIR_USD_DATA[PAIR_USD_DATA.length - 1][1];
  const latestPairUSDTimestamp = PAIR_USD_DATA[PAIR_USD_DATA.length - 1][0];

  const tickerNames: (string | number)[][] = [];
  tickerNames.push(['JPY', rates.JPY]);
  tickerNames.push(['XOF', rates.XOF]);
  tickerNames.push(['BTC', rates.BTC]);
  tickerNames.push(['ETH', rates.ETH]);
  tickerNames.push(['ETC', rates.ETC]);

  for (var j = 0; j < tickerNames.length; j++) {
    const currTicker = tickerNames[j];
    const tickerName = currTicker[0];
    const exchangeRate = Number(currTicker[1]);

    const currDataSet = await getPairDataFromS3(`pair${tickerName}.json`);
    const newPrice = latestPairUSDPrice * exchangeRate;
    currDataSet.push([latestPairUSDTimestamp, newPrice]);

    const params = {
      Bucket: 'pair-prices',
      Key: `pair${tickerName}.json`,
      Body: JSON.stringify(currDataSet),
    };

    s3.upload(params, function (err, data) {
      if (err) {
        throw err;
      }
      console.log(`File uploaded successfully. ${data.Location}`);
    });
  }
}

// async function main() {
//   await updateMetalData();

//   await calculatePairUSD();
//   await calculatePairPrices();
// }

// main();

exports.handler = async () => {
  await updateMetalData();

  await calculatePairUSD();
  await calculatePairPrices();
};
