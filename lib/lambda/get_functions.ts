import aws from 'aws-sdk';
import fetch from 'node-fetch';
// import 'dotenv/config';

aws.config.update({
  // accessKeyId: process.env.NEXT_PUBLIC_ACCESS_KEY_ID,
  // secretAccessKey: process.env.NEXT_PUBLIC_SECRET_ACCESS_KEY,
  accessKeyId: process.env['ACCESS_KEY_ID'],
  secretAccessKey: process.env['SECRET_ACCESS_KEY'],
  region: 'us-east-1',
});

const s3 = new aws.S3({ apiVersion: '2006-03-01' });

export async function getPairDataFromS3(filename: string) {
  try {
    const params = {
      Bucket: 'pair-prices',
      Key: filename,
    };

    const data = await s3.getObject(params).promise();

    const value = JSON.parse(data.Body!.toString('utf-8'));
    return value;
  } catch (e) {
    throw new Error(`Could not retrieve file from S3: ${e}`);
  }
}

export async function getMetalDataFromS3(filename: string) {
  try {
    const params = {
      Bucket: 'pair-prices/metals',
      Key: filename,
    };

    const data = await s3.getObject(params).promise();

    const value = JSON.parse(data.Body!.toString('utf-8'));
    return value;
  } catch (e) {
    throw new Error(`Could not retrieve file from S3: ${e}`);
  }
}

export async function getLatestMetalData() {
  const headers = {};
  // headers['x-access-token'] = process.env.GOLD_API_KEY;
  headers['x-access-token'] = process.env['GOLD_API_KEY'];
  headers['Content-Type'] = 'application/json';

  const goldResponse = await fetch('https://www.goldapi.io/api/XAU/USD', {
    method: 'GET',
    headers: headers,
  });
  const goldData: any = await goldResponse.json();

  const silverResponse = await fetch('https://www.goldapi.io/api/XAG/USD', {
    method: 'GET',
    headers: headers,
  });
  const silverData: any = await silverResponse.json();

  const metalData = {
    lastGoldPrice: goldData.price_gram_24k,
    goldTimestamp: goldData.timestamp,
    lastSilverPrice: silverData.price_gram_24k,
    silverTimestamp: silverData.timestamp,
  };

  return metalData;
}

export async function getExchangeRates() {
  //coinbase doesn't seem to be returning accurate XOF exchange rate
  const coinbaseResponse = await fetch(
    'https://api.coinbase.com/v2/exchange-rates?currency=USD'
  );

  //using this endpoint to get XOF exchange rate
  const exchangeRateHostResponse = await fetch(
    'https://api.exchangerate.host/latest?base=USD&symbols=XOF'
  );

  const coinbaseObj: any = await coinbaseResponse.json();
  const exchangeRateHostObj: any = await exchangeRateHostResponse.json();

  const rates = {
    BTC: Number(coinbaseObj.data.rates.BTC),
    ETC: Number(coinbaseObj.data.rates.ETC),
    ETH: Number(coinbaseObj.data.rates.ETH),
    JPY: Number(coinbaseObj.data.rates.JPY),
    XOF: Number(exchangeRateHostObj.rates.XOF),
  };

  return rates;
}
