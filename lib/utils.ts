import fetch from 'node-fetch';

export const kebabCase = (str) =>
  str &&
  str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map((x) => x.toLowerCase())
    .join('-');

export async function getLatestMetalData() {
  const headers = {};
  headers['x-access-token'] = process.env.NEXT_PUBLIC_GOLD_API_KEY;
  headers['Content-Type'] = 'application/json';

  const goldResponse = await fetch('https://www.goldapi.io/api/XAU/USD', {
    method: 'GET',
    headers: headers,
  });
  const goldData: any = await goldResponse.json();

  const silverResponse = await fetch('https://www.goldapi.io/api/XAG/USD', {
    method: 'GET',
    headers: headers,
  });
  const silverData: any = await silverResponse.json();

  const metalData = {
    lastGoldGramPrice: Number(goldData.price_gram_24k),
    goldTimestamp: Number(goldData.timestamp),
    lastSilverGramPrice: Number(silverData.price_gram_24k),
    silverTimestamp: Number(silverData.timestamp),
  };

  return metalData;
}

// @params year-month-day
// returns object with gold and silver prices, each of which is array of array
// in each array [0] is date and [1] is price
// using https://data.nasdaq.com/data/LBMA-london-bullion-market-association/usage/quickstart/api
async function getHistoricalMetalData(startDate: string, endDate: string) {
  const apiKey = process.env.NEXT_PUBLIC_NASDAQ_API_KEY;

  const goldResponse = await fetch(
    `/api/gold_proxy?startDate=${startDate}&endDate=${endDate}&apiKey=${apiKey}`,
    {
      method: 'GET',
    }
  );
  const goldData = await goldResponse.json();

  const silverResponse = await fetch(
    `/api/silver_proxy?startDate=${startDate}&endDate=${endDate}&apiKey=${apiKey}`,
    {
      method: 'GET',
    }
  );
  const silverData = await silverResponse.json();

  const historicalMetalData: {
    goldDatesPrices: (string | number)[][];
    silverDatesPrices: (string | number)[][];
  } = {
    goldDatesPrices: goldData.dataset.data,
    silverDatesPrices: silverData.dataset.data,
  };
  return historicalMetalData;
}

export async function getExchangeRates() {
  // coinbase doesn't seem to be returning accurate XOF exchange rate
  const coinbaseResponse = await fetch(
    'https://api.coinbase.com/v2/exchange-rates?currency=USD'
  );

  // using this endpoint to get XOF exchange rate
  const exchangeRateHostResponse = await fetch(
    'https://api.exchangerate.host/latest?base=USD&symbols=XOF'
  );

  const coinbaseObj: any = await coinbaseResponse.json();
  const exchangeRateHostObj: any = await exchangeRateHostResponse.json();

  const rates = {
    BTC: Number(coinbaseObj.data.rates.BTC),
    ETC: Number(coinbaseObj.data.rates.ETC),
    ETH: Number(coinbaseObj.data.rates.ETH),
    EUR: Number(coinbaseObj.data.rates.EUR),
    JPY: Number(coinbaseObj.data.rates.JPY),
    XOF: Number(exchangeRateHostObj.rates.XOF),
  };

  return rates;
}

export async function getHistoricalPairPrices(baseCurrency: string) {
  const HISTORICAL_PAIR_PRICES: number[][] = [];

  const exchangeRates = await getExchangeRates();

  let startDate = '2000-01-01';
  let exchangeRate = 1;

  switch (baseCurrency) {
  case 'EUR':
    startDate = '2000-01-01';
    exchangeRate = exchangeRates.EUR;
    break;
  case 'JPY':
    startDate = '2000-01-01';
    exchangeRate = exchangeRates.JPY;
    break;
  case 'XOF':
    startDate = '2000-01-01';
    exchangeRate = exchangeRates.XOF;
    break;
  case 'BTC':
    startDate = '2009-01-03';
    exchangeRate = exchangeRates.BTC;
    break;
  case 'ETH':
    startDate = '2015-07-30';
    exchangeRate = exchangeRates.ETH;
    break;
  case 'ETC':
    startDate = '2015-07-30';
    exchangeRate = exchangeRates.ETC;
    break;
  default:
    startDate = '2000-01-01';
    exchangeRate = 1;
    break;
  }

  const timestamp = new Date();
  const endDate = `${timestamp.getFullYear()}-${
    timestamp.getMonth() + 1
  }-${timestamp.getDate()}`;

  const historicalMetalPrices = await getHistoricalMetalData(
    startDate,
    endDate
  );

  const goldHistoricalPrices = historicalMetalPrices.goldDatesPrices;
  const silverHistoricalPrices = historicalMetalPrices.silverDatesPrices;

  // TODO: Fix mismatching data lengths
  // if (goldHistoricalPrices.length === silverHistoricalPrices.length) {
  for (let i = goldHistoricalPrices.length - 1; i >= 0; i--) {
    const goldPriceOz = Number(goldHistoricalPrices[i][1]);
    const silverPriceOz = Number(silverHistoricalPrices[i][1]);
    const goldSilverRatio = +(goldPriceOz / silverPriceOz).toFixed(2);

    const goldPriceGram = goldPriceOz / 31.103;
    const silverPriceGram = silverPriceOz / 31.103;

    const pairPriceUSD =
      (1 / goldSilverRatio) * goldPriceGram +
      (1 - 1 / goldSilverRatio) * silverPriceGram;

    let currDate = goldHistoricalPrices[i][0];
    // @ts-expect-error
    currDate = currDate.split('-');
    const date = new Date(currDate[0], currDate[1] - 1, currDate[2]);

    HISTORICAL_PAIR_PRICES.push([date.getTime(), pairPriceUSD * exchangeRate]);
  }
  // } else {
  //   console.log(goldHistoricalPrices.length);
  //   console.log(silverHistoricalPrices.length);
  //   throw new Error('gold and silver data lengths do not match');
  // }

  return HISTORICAL_PAIR_PRICES;
}
