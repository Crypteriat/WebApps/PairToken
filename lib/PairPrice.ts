import fs from 'fs';
import fetch from 'node-fetch';
import ratioData from '../data/chart/metals/ratio.json';
import goldGramData from '../data/chart/metals/goldGram.json';
import silverGramData from '../data/chart/metals/silverGram.json';
import pairUSD from '../data/chart/pairUSD.json';

function calculatePairUSD() {
  const PAIR_USD_ARRAY: (string | number)[][] = [];

  if (
    ratioData.length === goldGramData.length &&
    ratioData.length === silverGramData.length
  ) {
    for (var i = 0; i < ratioData.length; i++) {
      const currRatio = ratioData[i];
      const currGoldGram = goldGramData[i];
      const currSilverGram = silverGramData[i];

      const ratioDate = currRatio[0];
      const goldDate = currGoldGram[0];
      const silverDate = currSilverGram[0];

      if (ratioDate === goldDate && ratioDate === silverDate) {
        const ratio = currRatio[1];
        const goldPrice = currGoldGram[1];
        const silverPrice = currSilverGram[1];

        //PAIR gram = {  [1/ (current gold silver ratio) ]  x  current price of 1 gram of gold } + { [ 1 -  (1 /  (current gold silver ratio)  ]  x  current $ 1 gram of silver }
        const pairPrice =
          (1 / ratio) * goldPrice + (1 - 1 / ratio) * silverPrice;

        PAIR_USD_ARRAY.push([ratioDate, pairPrice]);
      } else {
        console.error(
          'ratioDate does not equal goldDate or silverDate\n ratioDate is ' +
            ratioDate +
            ', goldDate is ' +
            goldDate +
            ', silverDate is ' +
            silverDate
        );
      }
    }
  } else {
    console.error(
      'ratioData.length does not eqaul length of goldGramData or silverGram Data\n ratioData.length is ' +
        ratioData.length +
        ', goldGramData.length is ' +
        goldGramData.length +
        ', silverGramData.length is ' +
        silverGramData.length
    );
  }

  const PAIR_USD_JSON = JSON.stringify(PAIR_USD_ARRAY);
  fs.writeFile('./data/chart/pairUSD.json', PAIR_USD_JSON, 'utf8', (err) => {
    if (err) console.log(err);
    else {
      console.log('./data/chart/pairUSD.json written successfully\n');
    }
  });
}

async function getExchangeRates() {
  //coinbase doesn't seem to be returning accurate XOF exchange rate
  const coinbaseResponse = await fetch(
    'https://api.coinbase.com/v2/exchange-rates?currency=USD'
  );

  //using this endpoint to get XOF exchange rate
  const exchangeRateHostResponse = await fetch(
    'https://api.exchangerate.host/latest?base=USD&symbols=XOF'
  );

  const coinbaseObj: any = await coinbaseResponse.json();
  const exchangeRateHostObj: any = await exchangeRateHostResponse.json();

  const rates = {
    BTC: Number(coinbaseObj.data.rates.BTC),
    ETC: Number(coinbaseObj.data.rates.ETC),
    ETH: Number(coinbaseObj.data.rates.ETH),
    EUR: Number(coinbaseObj.data.rates.EUR),
    JPY: Number(coinbaseObj.data.rates.JPY),
    XOF: Number(exchangeRateHostObj.rates.XOF),
  };

  return rates;
}

async function calculatePairs() {
  const rates = await getExchangeRates();

  let tickerNames: (string | number)[][] = [];
  tickerNames.push(['EUR', rates.EUR]);
  tickerNames.push(['JPY', rates.JPY]);
  tickerNames.push(['XOF', rates.XOF]);
  tickerNames.push(['BTC', rates.BTC]);
  tickerNames.push(['ETH', rates.ETH]);
  tickerNames.push(['ETC', rates.ETC]);
  tickerNames.push(['BNB', 0.004516]);

  for (var j = 0; j < tickerNames.length; j++) {
    let currTicker = tickerNames[j];
    let tickerName = currTicker[0];
    let tickerPrice = Number(currTicker[1]);

    let values: (string | number)[][] = [];

    for (var i = 0; i < pairUSD.length; i++) {
      let currPair = pairUSD[i];
      let currDate = currPair[0];
      let currPrice = currPair[1];

      let tPrice = currPrice * tickerPrice;

      values.push([currDate, tPrice]);
    }

    let pairJsonFile = JSON.stringify(values);
    fs.writeFile(
      './data/chart/pair' + tickerName + '.json',
      pairJsonFile,
      'utf8',
      (err) => {
        if (err) console.log(err);
        else {
          console.log(
            './data/chart/pair' + tickerName + '.json written successfully\n'
          );
        }
      }
    );
  }
}

async function main() {
  // await getExchangeRates();
  calculatePairUSD();
  await calculatePairs();
}

main();
