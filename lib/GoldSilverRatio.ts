import fs from 'fs';
import goldData from '../data/chart/metals/gold.json';
import silverData from '../data/chart/metals/silver.json';

function calculateGoldSilverRatio() {
  const GOLD_DATA = goldData.dataset.data;
  const SILVER_DATA = silverData.dataset.data;
  const GOLD_SILVER_RATIO_ARRAY: (string | number)[][] = [];

  for (var i = GOLD_DATA.length - 1; i >= 0; i--) {
    const currGold = GOLD_DATA[i];
    const currSilver = SILVER_DATA[i];

    const goldDate = currGold[0];
    const silverDate = currSilver[0];

    //If dates match
    if (goldDate === silverDate) {
      const goldPrice = currGold[1];
      const silverPrice = currSilver[1];

      //If price values are not null nor string
      if (typeof goldPrice === 'number' && typeof silverPrice === 'number') {
        //Calculate gold silver ratio data and append array
        const currGoldSilverRatio = +(goldPrice / silverPrice).toFixed(2);
        //@ts-expect-error
        let tempDate = goldDate.split('-');
        const timestamp = new Date(tempDate[0], tempDate[1] - 1, tempDate[2]);

        GOLD_SILVER_RATIO_ARRAY.push([
          timestamp.getTime(),
          currGoldSilverRatio,
        ]);
      } else {
        console.error(
          'goldPrice or silverPrice is not number. goldPrice is ' +
            typeof goldPrice +
            ' and silverPrice is ' +
            typeof silverPrice
        );
      }
    } else {
      console.error(
        'dates no dont match: gold - ' + goldDate + ', silver - ' + silverDate
      );
    }
  }

  const GOLD_SILVER_RATIO_JSON = JSON.stringify(GOLD_SILVER_RATIO_ARRAY);
  fs.writeFile(
    './data/chart/metals/ratio.json',
    GOLD_SILVER_RATIO_JSON,
    'utf8',
    (err) => {
      if (err) console.log(err);
      else {
        console.log('./data/chart/metals/ratio.json written successfully\n');
      }
    }
  );
}

calculateGoldSilverRatio();
