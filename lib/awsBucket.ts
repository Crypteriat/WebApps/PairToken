import aws from 'aws-sdk';

aws.config.update({
  accessKeyId: process.env.NEXT_PUBLIC_ACCESS_KEY_ID,
  secretAccessKey: process.env.NEXT_PUBLIC_SECRET_ACCESS_KEY,
  region: 'us-east-1',
});

const s3 = new aws.S3({ apiVersion: '2006-03-01' });

export async function getPairData(filename: string) {
  try {
    const params = {
      Bucket: 'pair-prices',
      Key: filename,
    };

    const data = await s3.getObject(params).promise();

    const value = JSON.parse(data.Body!.toString('utf-8'));
    return value;
  } catch (e) {
    throw new Error(`Could not retrieve file from S3: ${e}`);
  }
}
