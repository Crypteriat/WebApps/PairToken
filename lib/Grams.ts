import fs from 'fs';
import goldData from '../data/chart/metals/gold.json';
import silverData from '../data/chart/metals/silver.json';

export function convertGoldToGrams() {
  const GOLD_DATA = goldData.dataset.data;
  const GOLD_GRAMS_ARRAY: (string | number)[][] = [];

  for (var i = GOLD_DATA.length - 1; i >= 0; i--) {
    const currGold = GOLD_DATA[i];
    const goldDate = currGold[0];
    const goldPrice = currGold[1];

    if (typeof goldPrice === 'number' && goldDate !== null) {
      const currGoldGramPrice = goldPrice / 31.1;

      //@ts-expect-error
      let tempDate = goldDate.split('-');
      const timestamp = new Date(tempDate[0], tempDate[1] - 1, tempDate[2]);

      GOLD_GRAMS_ARRAY.push([timestamp.getTime(), currGoldGramPrice]);
    } else {
      console.error(
        'goldPrice or goldDate is not number or string. goldPrice is ' +
          typeof goldPrice +
          ' and goldDate is ' +
          typeof goldDate
      );
    }
  }

  const GOLD_GRAM_JSON = JSON.stringify(GOLD_GRAMS_ARRAY);
  fs.writeFile('./data/chart/metals/goldGram.json', GOLD_GRAM_JSON, 'utf8', (err) => {
    if (err) console.log(err);
    else {
      console.log('./data/chart/metals/goldGram.json written successfully\n');
    }
  });
}

export function convertSilverToGrams() {
  const SILVER_DATA = silverData.dataset.data;
  const SILVER_GRAMS_ARRAY: (string | number)[][] = [];

  for (var i = SILVER_DATA.length - 1; i >= 0; i--) {
    const currSilver = SILVER_DATA[i];
    const silverDate = currSilver[0];
    const silverPrice = currSilver[1];

    if (typeof silverPrice === 'number' && silverDate !== null) {
      const currSilverGramPrice = silverPrice / 31.1;

      //@ts-expect-error
      let tempDate = silverDate.split('-');
      const timestamp = new Date(tempDate[0], tempDate[1] - 1, tempDate[2]);

      SILVER_GRAMS_ARRAY.push([timestamp.getTime(), currSilverGramPrice]);
    } else {
      console.error(
        'silverPrice or silverDate is not number or string. silverPrice is ' +
          typeof silverPrice +
          ' and silverDate is ' +
          typeof silverDate
      );
    }
  }

  const SILVER_GRAM_JSON = JSON.stringify(SILVER_GRAMS_ARRAY);
  fs.writeFile(
    './data/chart/metals/silverGram.json',
    SILVER_GRAM_JSON,
    'utf8',
    (err) => {
      if (err) console.log(err);
      else {
        console.log('./data/chart/metals/silverGram.json written successfully\n');
      }
    }
  );
}

function main() {
  convertGoldToGrams();
  convertSilverToGrams();
}

main();
